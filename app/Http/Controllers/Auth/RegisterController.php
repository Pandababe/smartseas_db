<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Role;
use Session;
use Mail;
use App\Mail\Credentials;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    public $email;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['roles']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'  => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name'                => $data['name'],
            'email'               => $data['email'],
            'password'            => Hash::make($data['password']),
            'partner_admin_image' => '/assets/static/images/user.png',
            'roles'               => 'guest'
        ]);
        $user->roles()->attach(Role::where('name', 'guest')->first());
        return $user;

    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        $name            = $request->input('name');
        $this->email     = $request->input('email');
        $password        = $request->input('password');

        $mail = 'mateojulius98@yahoo.com';

        $data = array(
            'name'      => $name,
            'password'  => $password,
            'email'     => $this->email,
            'url'       => env('APP_URL'). '/login'
        );
        Mail::send('mail.credentials', ['data' => $data ], function($message) use ($data)
        {
            $message->to($this->email)
                ->subject("Your smartseas credentials!");
        });
        Session::flash('message', 'Successfully registered! Email sent for the credentials');
        return redirect('/register');
    }
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Auth;
use App\User;
use Crypt;
use Illuminate\Support\Facades\Hash;
use Session;


class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     
    public function index()
    {
        //
        $user = DB::table('users')->where('id', Auth::user()->id )->get()->first();
        return view('dashboard.profile.update')->with('user', $user);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        $user->partner_admin_ID = $request->get('partner_admin_ID');
        $user->partner_name = $request->get('partner_name');
        $user->partner_name_address = $request->get('partner_name_address');
        $user->partner_admin = $request->get('partner_admin');
        $user->partner_admin_name = $request->get('partner_admin_name');
        $user->partner_admin_address = $request->get('partner_admin_address');
        $user->partner_admin_gender = $request->get('partner_admin_gender');
        $user->partner_admin_position = $request->get('partner_admin_position');
        

        // $this->validate($request, [
        //     'partner_admin_image' => '|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        //     'password' => 'required'
        // ]);
    
        if ($request->hasFile('partner_admin_image')) {
            $image = $request->file('partner_admin_image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('avatars');
            $image->move($destinationPath, $name);
            $user->partner_admin_image = '/avatars/' . $name; 
        }

        if (  Hash::make($request->get('old_password')) == Auth::user()->password  ) {
            echo 'true';
        } else {
            
        }

        $user->save();
        Session::flash('message', 'Successfully updated!');
        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/', function () {
        return view('welcome')->with('title', 'Smartseas PH');
    });

    Auth::routes();
    Route::get('logout', 'Auth\LoginController@logout');

    Route::get('dashboard', 'DashboardController@index');

    Route::get('profile', 'ProfileController@index');
    Route::post('profile/update', 'ProfileController@update');

    Route::group(['middleware' => 'roles', 'roles'=>['admin']], function () {
        Route::post('register', 'Auth\RegisterController@register')->name('register');
        Route::get('register', 'Auth\RegisterController@showRegistrationForm');
        Route::resource('usermanagement', 'UserManagementController');
    });
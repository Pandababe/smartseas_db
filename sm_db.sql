-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2018 at 04:31 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sm_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_name_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_admin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_admin_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_admin_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_admin_gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_admin_position` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_admin_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_admin_ID` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_status` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `partner_name`, `partner_name_address`, `partner_admin`, `partner_admin_name`, `partner_admin_address`, `partner_admin_gender`, `partner_admin_position`, `partner_admin_image`, `partner_admin_ID`, `partner_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Julius', 'partanduls@gmail.com', '$2y$10$.0tmaWJ2JaW.WESZctTe0ehu.q/z6T8xIix5qFmMPI8oldq0e0.BC', 'haha', NULL, NULL, NULL, NULL, 'Female', NULL, '/assets/static/images/user.png', '1234', 2, 's1nmHGNEY3WiKuBVGbK2cYGNlfMJq1sO8c049BlUTeq7RBqBRkMevx5jPymo', '2018-07-16 21:25:34', '2018-07-16 21:28:42'),
(2, 'chona', 'chona@gmail.com', '$2y$10$4yjqfNX92ToDfWSpG0bYwup.tmRbFR52kcbFWHNjMjnUUTAkmoBi.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '/assets/static/images/user.png', NULL, 0, 'i0mcr6Cu7ybQcB1uNf6k2flkJqc7AeOlTDrqwYFdd5aFD8lKS5cyoNQX9dfB', '2018-07-16 21:29:09', '2018-07-16 21:29:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

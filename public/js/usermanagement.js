
document.querySelectorAll(".action_temp").forEach(
    elx => {
        elx.addEventListener("click", e => {
            var mode = e.target.getAttribute('data-mode');
            if (mode == 'approve') {
                document.querySelectorAll('.form_approve, .form_decline').forEach(
                    el => { el.style.display = "inline-block";
                })
            } else if (mode == 'decline') {
                document.querySelector(".form_approve").style.display = "none";
                document.querySelector(".form_decline").style.display = "inline-block";
            }
        });
    }
)

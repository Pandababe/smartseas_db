@if (Route::has('login'))

    @auth

                <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
                <span>Copyright © 2017 Designed by <a href="https://colorlib.com/" target="_blank" title="BlueInspires">BlueInspires</a>. All rights reserved.</span><!-- Global site tag (gtag.js) - Google Analytics -->
                <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
                <script>
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());
                    
                    gtag('config', 'UA-23581568-13');
                </script>
                </footer>
    @else

    @endauth
@endif


         </div>
      </div>

      <script type="text/javascript" src="{{ asset('js/vendor.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/bundle.js') }}"></script>
      @if (Route::is('usermanagement.*'))
      <script type="text/javascript" src="{{ asset('js/usermanagement.js') }}"></script>
      @elseif (Route::name('register'))
      <script type="text/javascript" src="{{ asset('js/register.js') }}"></script>
      @endif
      
   </body>
</html>
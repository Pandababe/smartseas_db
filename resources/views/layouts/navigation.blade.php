<div>
         <div class="sidebar">
            <div class="sidebar-inner">
               <div class="sidebar-logo">
                  <div class="peers ai-c fxw-nw">
                     <div class="peer peer-greed">
                        <a class="sidebar-link td-n" href="index.html" class="td-n">
                           <div class="peers ai-c fxw-nw">
                              <div class="peer">
                                 <div class="logo"><img src="/assets/static/images/smartseas-logo.jpg" alt="" style="width: 100%;"></div>
                              </div>
                              <div class="peer peer-greed">
                                 <h5 class="lh-1 mB-0 logo-text">Smartseas</h5>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="peer">
                        <div class="mobile-toggle sidebar-toggle"><a href="#" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
                     </div>
                  </div>
               </div>
               <ul class="sidebar-menu scrollable pos-r">
                  <li class="nav-item mT-30 active"><a class="sidebar-link" href="{{ URL::to('/')}}/dashboard" default><span class="icon-holder"><i class="c-blue-500 ti-home"></i> </span><span class="title">Dashboard</span></a></li>
                  
                  @if (Auth::user()->roles == 'admin' )
                  
                  <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);"><span class="icon-holder"><i class="c-pink-500 ti-user"></i> </span><span class="title">User Management</span><span class="arrow"><i class="ti-angle-right"></i></span></a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="{{ URL::to('/')}}/register">Add User</a></li>
                        <li><a class="sidebar-link" href="{{ URL::to('/')}}/usermanagement">User List</a></li>
                    </ul>
                  </li>
                
                  @endif

                  <li class="nav-item dropdown">
                     <a class="dropdown-toggle" href="javascript:void(0);"><span class="icon-holder"><i class="c-orange-500 ti-settings"></i> </span><span class="title">Settings</span> <span class="arrow"><i class="ti-angle-right"></i></span></a>
                     <ul class="dropdown-menu">
                        @if (Auth::user()->roles == 'user' )
                        <li><a class="sidebar-link" href="{{ URL::to('/')}}/general">General</a></li>
                        @endif
                        <li><a class="sidebar-link" href="{{ URL::to('/')}}/profile">Profile</a></li>
                        <li><a class="sidebar-link" href="{{ URL::to('/')}}/logout">Logout</a></li>
                     </ul>
                  </li>
                 
               </ul>
            </div>
         </div>
         <div class="page-container">
            <div class="header navbar">
               <div class="header-container">
                  <ul class="nav-left">
                     <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
                  </ul>
                  <ul class="nav-right">
                     <li class="notifications dropdown">
                        <span class="counter bgc-red">3</span> <a href="#" class="dropdown-toggle no-after" data-toggle="dropdown"><i class="ti-bell"></i></a>
                        <ul class="dropdown-menu">
                           <li class="pX-20 pY-15 bdB"><i class="ti-bell pR-10"></i> <span class="fsz-sm fw-600 c-grey-900">Notifications</span></li>
                           <li>
                              <ul class="ovY-a pos-r scrollable lis-n p-0 m-0 fsz-sm">
                                
                                 <li>
                                    <a href="#" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                       <div class="peer mR-15"><img class="w-3r bdrs-50p" src="{{ URL::to('/') }}/assets/static/images/user.png" alt=""></div>
                                       <div class="peer peer-greed">
                                          <span><span class="fw-500">Lee Doe</span> <span class="c-grey-600">commented on your <span class="text-dark">video</span></span></span>
                                          <p class="m-0"><small class="fsz-xs">10 mins ago</small></p>
                                       </div>
                                    </a>
                                 </li>
                              </ul>
                           </li>
                           <li class="pX-20 pY-15 ta-c bdT"><span><a href="#" class="c-grey-600 cH-blue fsz-sm td-n">View All Notifications <i class="ti-angle-right fsz-xs mL-10"></i></a></span></li>
                        </ul>
                     </li>
                     
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                           <div class="peer mR-10"><img class="w-2r bdrs-50p" src="{{ Auth::user()->partner_admin_image }}" alt=""></div>
                           <div class="peer"><span class="fsz-sm c-grey-900">{{ Auth::user()->name }}</span></div>
                        </a>
                        <ul class="dropdown-menu fsz-sm">
                           
                           <li><a href="{{ URL::to('/')}}/profile" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-user mR-10"></i> <span>Profile</span></a></li>
                           <li role="separator" class="divider"></li>
                           <li><a href="{{ URL::to('/')}}/logout" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-power-off mR-10"></i> <span>Logout</span></a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
</div>
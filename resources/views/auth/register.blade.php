@extends('layouts.dashboard')


@section('content')

    <div class="peers ai-s fxw-nw h-100vh">
         <div class="col-12 col-md-6 peer pX-40 pY-80 h-100 bgc-white scrollable pos-r authentication">
            <h4 class="fw-300 c-grey-900 mB-40">Register</h4>
            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        {!! csrf_field() !!}
                        
               <div class="form-group">
                   <label class="text-normal text-dark">Username</label>
                  
                   <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                  
                   @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                   
                    @endif

                </div>
               <div class="form-group">
                   <label class="text-normal text-dark">Email Address</label>
                   
                   <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                   
                   @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                </div>
               <div class="form-group">
                   <label class="text-normal text-dark">Password</label>
                    <div class="row">
                        <div class="col-md-8">
                            <input style="margin-bottom: 10px;" id="password" type="text" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        </div>
                        <div class="col-md-4">
                            <a href="javascript:void(0);" onClick="generateKey()" class="btn btn-primary">Generate</a>
                        </div>
                    </div>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
               </div>
               <div class="form-group">
                  <div class="peers ai-c jc-sb fxw-nw">
                     <div class="peer"><button class="btn btn-primary">Register</button></div>
                  </div>
               </div>
            </form>
            @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
            @endif
        </div>
    </div>

@stop
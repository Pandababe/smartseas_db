<div class="modal fade" id="action1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Action</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form_approve" style="display: none" action="{{action('UserManagementController@update',['id'=>$user->id])}}" method="post">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <input type="hidden" name="action" value="Approve">
            <button type="submit" class="btn btn-success">Approve</button>
        </form>
        <form class="form_decline" style="display: none" action="{{action('UserManagementController@update',['id'=>$user->id])}}" method="post">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <input type="hidden" name="action" value="Decline">
            <button type="submit" class="btn btn-danger">Decline</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
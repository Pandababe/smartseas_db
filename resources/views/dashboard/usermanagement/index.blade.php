@extends('layouts.dashboard')


@section('content')


@if(Session::has('message'))
    <p class="alert @if(Session::get('action')) alert-success @else alert-danger @endif">{{ Session::get('message') }}</p>
@endif

<div id="dataTable_wrapper" class="dataTables_wrapper">
   <table id="dataTable" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
      <thead>
         <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 217.5px;">Username</th>
            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 314.5px;">Partner Name</th>
            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 166.5px;">Partner Admin Name</th>   
            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 56.5px;">Partner Admin Position</th>
            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 140.5px;">Role</th>
            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 140.5px;">Status</th>
            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 140.5px;">Actions</th>
         </tr>
      </thead>
      <tfoot>
         <tr>
            <th rowspan="1" colspan="1">Username</th>
            <th rowspan="1" colspan="1">Partner Name</th>
            <th rowspan="1" colspan="1">Partner Admin Name</th>
            <th rowspan="1" colspan="1">Partner Admin Position</th>
            <th rowspan="1" colspan="1">Role</th>
            <th rowspan="1" colspan="1">Status</th>
            <th rowspan="1" colspan="1">Actions</th>
         </tr>
      </tfoot>
      <tbody>
          @foreach( $users as $user )
            @if ( !$user->hasRole('admin'))
            <tr role="row" class="odd">
                <td class="sorting_1"><a href="{{ URL::to('usermanagement/' . $user->id . '/edit') }}">{{ $user->name }}</a></td>
                <td>{{ $user->partner_name }}</td>
                <td>{{ $user->partner_admin_name }}</td>
                <td>{{ $user->partner_admin_position }}</td>
                <td>{{ $user->hasRole('user') ? 'User' : 'Guest' }}</td>
                <td>
                    @if ( $user->roles == 'user' || $user->roles == 'admin' )
                    <div class="alert alert-success mB-0" role="alert">Active</div>
                    @else
                    <div class="alert alert-danger mB-0" role="alert">Pending</div>
                    @endif
                </td>
                <td>
                      <a class="btn btn-primary" href="{{ URL::to('usermanagement/' . $user->id . '/edit') }}">Settings</a>
                </td>
            </tr>
            @endif
            
        @endforeach
      </tbody>
   </table>
</div>
@stop
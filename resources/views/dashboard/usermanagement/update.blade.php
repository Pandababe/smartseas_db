@extends('layouts.dashboard')
@section('content')

@if($user)
    <div class="approve_section">
        <p class="image_approve"><img class="img_res" src="{{ $user->partner_admin_image }}" alt=""></p>
        <div class="row">
        <div class="col-md-2">
        @if ( $user->hasRole('user') || $user->hasRole('admin') )
            <div class="alert alert-success mB-0" role="alert">Active</div>
        @else
            <div class="alert alert-danger mB-0" role="alert">Pending</div>
        @endif
        </div>
        </div>
        <table>
            <tr>
                <td><b>Username: </b></td>
                <td>{{ $user->name }}</td>
            </tr>
            <tr>
                <td><b>Email: </b></td>
                <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
            </tr>
            <tr>
                <td><b>Partner Name: </b></td>
                <td>{{ $user->partner_name }}</td>
            </tr>
            <tr>
                <td><b>Partner Name Address: </b></td>
                <td>{{ $user->partner_name_address }}</td>
            </tr>
            <tr>
                <td><b>Partner Admin: </b></td>
                <td>{{ $user->partner_admin }}</td>
            </tr>
            <tr>
                <td><b>Partner Admin ID: </b></td>
                <td>{{ $user->partner_admin_ID }}</td>
            </tr>
            <tr>
                <td><b>Partner Admin Name: </b></td>
                <td>{{ $user->partner_admin_name }}</td>
            </tr>
            <tr>
                <td><b>Partner Admin Address: </b></td>
                <td>{{ $user->partner_admin_address }}</td>
            </tr>
            <tr>
                <td><b>Partner Admin Gender: </b></td>
                <td>{{ $user->partner_admin_gender }}</td>
            </tr>
            <tr>
                <td><b>Partner Admin Position: </b></td>
                <td>{{ $user->partner_admin_position }}</td>
            </tr>
            <tr>
                <td>                    
                    @if ($user->hasRole('guest'))
                      <button type="button" data-toggle="modal" data-mode="approve" data-target="#action1" class="btn btn-success action_temp">Approve</button>
                    @endif
                      <button type="button" data-toggle="modal" data-mode="decline" data-target="#action1" class="btn btn-danger action_temp">Decline</button>
                </td>
            </tr>
            <tr>
                <td><button type="button" data-toggle="modal" data-target="#action2" class="btn btn-danger">Delete</button></td>
            </tr>
        </table>
    </div>
@else 
    <h1><center>User Not Available</center></h1>
@endif
@include('dashboard.usermanagement.updatemodal')
@include('dashboard.usermanagement.deletemodal')
@stop
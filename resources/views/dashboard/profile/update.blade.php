@extends('layouts.dashboard')


@section('content')

<div class="masonry-item col-md-12">
   <div class="bgc-white p-20 bd">
      <h6 class="c-grey-900">Edit Profile</h6>
      <div class="mT-30">
         <form method="POST" action="{{ action('ProfileController@update') }}" enctype="multipart/form-data">
             {{ csrf_field() }}
            <div class="form-row">
               <div class="form-group col-md-6">
                   <label for="input">Partner Admin ID</label>
                   <input type="text" name="partner_admin_ID" class="form-control" id="input" value="{{ $user->partner_admin_ID }}">
                </div>
               <div class="form-group col-md-6">
                   <label for="input0">Picture</label>
                   <input type="file" name="partner_admin_image" class="form-control" id="input0" value="{{ $user->partner_admin_image }}">
                </div>
               <div class="form-group col-md-6">
                   <label for="input1">Partner Name</label>
                   <input type="text" name="partner_name" class="form-control" id="input1" value="{{ $user->partner_name }}">
                </div>
          
                 <div class="form-group col-md-12">
                   <label for="input2">Partner Name Address</label>
                   <textarea name="partner_name_address" class="form-control" id="input2">{{ $user->partner_name_address }}</textarea>
                </div>
            </div>
            <div class="form-row">
               <div class="form-group col-md-6">
                   <label for="input3">Partner Admin</label>
                   <input type="text" name="partner_admin" class="form-control" id="input3" value="{{ $user->partner_admin }}">
                </div>
               <div class="form-group col-md-6">
                   <label for="input4">Partner Admin Name</label>
                   <input type="text" name="partner_admin_name" class="form-control" id="input4" value="{{ $user->partner_admin_name }}">
                </div>
        
                <div class="form-group col-md-6">
                  <label for="input6">Gender</label>
                  <select name="partner_admin_gender" id="input6" class="form-control">
                     <option @if($user->partner_admin_gender == 'Male') selected="selected" @endif>Male</option>
                     <option @if($user->partner_admin_gender == 'Female') selected="selected" @endif>Female<option>
                  </select>
               </div>
               <div class="form-group col-md-12">
                   <label for="input5">Partner Admin Address</label>
                   <textarea name="partner_admin_address" class="form-control" id="input5">{{ $user->partner_admin_address }}</textarea>
                </div>
               
            </div>
            <div class="form-row">
               <div class="form-group col-md-6">
                   <label for="input7">Partner Admin Position</label>
                   <input type="text" name="partner_admin_position" class="form-control" id="input7" value="{{ $user->partner_admin_position }}">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @elseif(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
            @endif
           
         </form>
      </div>
   </div>
</div>
@stop